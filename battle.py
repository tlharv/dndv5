from DND5e import *

# The global lists we'll use:
ML = []  # this is the MASTER LIST, the base reference for all combatants.
GG_picklist = []  # this is the list of good guys, names only.  Used for choosing opponents.
BG_picklist = []  # this is the list of good guys, names only.  Used for choosing opponents.

# Global constants
Die_Roll_Duration_secs = 2 # includes time to count the sum, which only plays in to rolling multiple dice


# Build list of good guy types
def ListGoodGuyTypes():
    x = 1
    for GGT in GG_Classes:
        print(x,GGT)
        x += 1
    print

def ListBadGuyTypes():
    x = 1
    for BGT in BG_Classes:
        print(x,BGT)
        x += 1
    print

# Create functions
def CreateSides(NumGG,NumBG):
    GG = []      # list of good guy instances of hero class
    BG = []      # list of bad guy instances of hero class
    global RL_setup_seconds
    global Die_Roll_Duration_secs

    # Define the good guys
    for x in range(0,NumGG):
        name = "Guard_" + str(x)
        dude = Guard()
        dude.Name = name
        dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
        RL_setup_seconds += Die_Roll_Duration_secs
        dude.hp = (dice(2,8) + modifier(dude.CON)) # roll individual hp
        RL_setup_seconds += Die_Roll_Duration_secs
        GG.append(dude) # unsorted list of all good guy instances


    # Define the bad guys
    for y in range(0,NumBG):
        name = "FireNewt_" + str(y)
        dude = FireNewt()
        dude.Name = name
        dude.INIT = dice(1,20) + modifier(dude.DEX) # roll new INIT
        RL_setup_seconds += Die_Roll_Duration_secs
        dude.hp = dice(2,8) + modifier(dude.CON) # roll individual hp
        RL_setup_seconds += Die_Roll_Duration_secs
        BG.append(dude) # unsorted list of all bad guy instances

    # Return the results
    return [GG,BG] #return a list of the two sorted lists


def printcombatpairs(CL):
    for GG in CL[0]:
        print(GG.INIT, GG.Name, GG.hp, GG.Opponents)
    for BG in CL[1]:
        print(BG.INIT, BG.Name, BG.hp, BG.Opponents)


def printHP(CL):
    print('Name      HP')
    for GG in CL[0]:
        print('{:8} {:3d}'.format(GG.Name,GG.hp))
    for BG in CL[1]:
        print('{:8} {:3d}'.format(BG.Name,BG.hp))


def ShowPairing(attacker,defender):
    print('The attacker is {:8} and the defender is {:8}'.format(attacker.Name, defender.Name))


def UpdateML_defender_hp(defender_name,damage):
    global ML

    # whether the defender is good or bad, update their hit points in the Master List.
    for x in range(0,len(ML[0])):
        if ML[0][x].Name == defender_name:
            hp_original = ML[0][x].hp
            hp_new = hp_original - damage
            ML[0][x].hp = hp_new
            print('      Good guy {:8} now has {:3d} hp'.format(ML[0][x].Name, ML[0][x].hp))
    for y in range(0,len(ML[1])):
        if ML[1][y].Name == defender_name:
            hp_original = ML[1][y].hp
            hp_new = hp_original - damage
            ML[1][y].hp = hp_new
            print('      Bad guy {:8} now has {:3d} hp'.format(ML[1][y].Name, ML[1][y].hp))


def UpdateCO_defender_hp(defender_name,damage): # update CombatOrder
    global CombatOrder

    # Search through CombatOrder for the defender name
    for dude in CombatOrder:
        if dude.Name == defender_name:
            dude.hp -= damage


def getDefHP(defender_name):
    global CombatOrder
    for dude in CombatOrder:
        if dude.Name == defender_name:
            return dude.hp


def GetCombatantAC(defender_name):
    global CombatOrder

    # Search through CO for the defender name
    for dude in CombatOrder:
        if dude.Name == defender_name:
            return dude.AC


def GetDefAC(defender_name):
    global ML

    for x in range(0,len(ML[0])):
        if ML[0][x].Name == defender_name:
            def_AC = ML[0][x].AC
            print('      {:8} has AC {:3d} and {:3d} hp'.format(ML[0][x].Name,def_AC, ML[0][x].hp))
    for y in range(0,len(ML[1])):
        if ML[1][y].Name == defender_name:
            def_AC = ML[1][y].AC
            print('      {:8} has AC {:3d} and {:3d} hp'.format(ML[1][y].Name,def_AC, ML[1][y].hp))
    return def_AC

def IsDead(defender_name):
    global CombatOrder

    for dude in CombatOrder:
        if dude.Name == defender_name:
            if dude.hp < 1:
                return True
            else:
                return False


def CheckDead(defender_name):
    global ML

    # whether the defender is good or bad, check to see if they're dead.
    for x in range(0,len(ML[0])):
        if ML[0][x].Name == defender_name:
            if ML[0][x].hp < 1:
                return True  # this guy is dead
            else:
                return False
    for y in range(0,len(ML[1])):
        if ML[1][y].Name == defender_name:
            if ML[1][y].hp < 1:
                return True
            else:
                return False

def RemoveOpponentFromML(attacker_name):
    global ML
    # When a combatant dies, they get removed from all Opponent lists.
    for GG in ML[0]:
        if attacker_name in GG.Opponents:
            # attacker was one of the good guys and he's in a BG.Opponent list somewhere.  Remove him from that list.
            for BG in ML[1]:
                if attacker_name in BG.Opponents:
                    BG.Opponents.remove(attacker_name)
                    print('Removed ' + attacker_name + ' from the opponent list of ' + BG.Name)
    for BG in ML[1]:
        if attacker_name in BG.Opponents:
            # attacker was one of the bad guys and he's in a GG.Opponent list somewhere.  Remove him from that list.
            for GG in ML[0]:
                if attacker_name in GG.Opponents:
                    GG.Opponents.remove(attacker_name)
                    print('Removed ' + attacker_name + ' from the opponent list of ' + GG.Name)


def ListCombatOrder(CombatOrder):
    print('There are {:3d} combatants.'.format(len(CombatOrder)))
    print('INIT   NPC Name          AC       HP')
    for attacker in CombatOrder:
        print('{:3d}    {:12}     {:3d}      {:3d}'.format(attacker.INIT, attacker.Name, attacker.AC, attacker.hp))
    print()

def convert_time(time):
    day = time // (24 * 3600)
    time = time % (24 * 3600)
    hour = time // 3600
    time %= 3600
    minutes = time // 60
    time %= 60
    seconds = time
    print("d:h:m:s-> %d:%d:%d:%d" % (day, hour, minutes, seconds))



#_________  MAIN PROGRAM BODY _________
print()
print('####################  N E W   C O M B A T  ####################\n')
print()
DoneSelecting = False
RL_setup_seconds = 0  # Running tally of the seconds in real life it would take to set up this combat
RL_combat_seconds = 0 # Running tally of the seconds in real life it would take to play out this combat

# Collect complement of Good Guy side
while not DoneSelecting:
    ListGoodGuyTypes()
    GG_setup_type = int(input('Add which Good Guy type? '))
    GG_setup_num = int(input('How many of that type of Good Guy are in the battle?'))
    DoneSel = input('Finished entering good guys ?')
    if DoneSel in ['y','Y','Yes','yes']:
        DoneSelecting = True

#ListGoodGuyTypes()
#ListBadGuyTypes()

# Get number of people on each side of the battle
GG_start = int(input('Number of good guys (minimum 1): '))
BG_start = int(input('Number of bad guys (minimum 1): '))
# GG_start = 1
# BG_start = 8

# Show everything?
ans = input('Verbose output? ')
if ans in ['y','Y','Yes,','yes']:
	verbose = True
else:
	verbose = False

if verbose:
        print('Verbose output chosen.')


# Update Master List of all combatants, sorted by their INIT roll
ML = CreateSides(GG_start, BG_start)
    # ML[0] = sorted list of all good guy instances
    # ML[1] = sorted list of all bad guy instances

# Set initial contents of picklist_names; we'll draw from these lists for opponent assignment.
BG_picklist_names = []
for BG in ML[1]:
    BG_picklist_names.append(BG.Name)

GG_picklist_names = []
for GG in ML[0]:
    GG_picklist_names.append(GG.Name)


# To start, randomly choose opponent pairs from the available fighter picklists.
# Update the ML record with the selected opponents.

if len(GG_picklist_names) < len(BG_picklist_names): # BG outnumber GG
    # First go through the good guys and assign a random opponent.
    for Gdude in ML[0]:
        random_BG_target_name = random.choice(BG_picklist_names)
        Gdude.Opponents.append(random_BG_target_name)
        for Bdude in ML[1]:  # now give that bad guy this good guy as an opponent
            if Bdude.Name == random_BG_target_name:
                Bdude.Opponents.append(Gdude.Name)
        BG_picklist_names.remove(random_BG_target_name)

    # Now assign an opponent to the extra bad guys
    for extra_BG in BG_picklist_names:  # Get a name from each of the extra bad guys
        for m in range(0,len(ML[1])):  # Search through the master list of bad guys.
            if ML[1][m].Name == extra_BG:  # if the name matches,
                random_GG_target = random.choice(GG_picklist_names)       # get a random good guy name
                ML[1][m].Opponents.append(random_GG_target)  # and assign it to the extra bad guy.
                # Now circle back to add these bad guy opponents to the good guys who are outnumbered.
                for n in range(0,len(ML[0])):  # Look through the master GG list
                    if ML[0][n].Name == random_GG_target:  # until you find the randomly-selected name
                        ML[0][n].Opponents.append(ML[1][m].Name) # add that BG to its opponent list.
                # GG_picklist_names.remove(random_GG_target) # so the GG has no more than 2 on 1
else:
    # This section is for when Good guys >= Bad guys.
    # First go through the bad guys and assign a random opponent.
    for Bdude in ML[1]:
        random_GG_target_name = random.choice(GG_picklist_names)
        Bdude.Opponents.append(random_GG_target_name)
        for Gdude in ML[0]:  # now give that good guy this bad guy as an opponent
            if Gdude.Name == random_GG_target_name:
                Gdude.Opponents.append(Bdude.Name)
        GG_picklist_names.remove(random_GG_target_name)

    # Now assign an opponent to the extra good guys
    for extra_GG in GG_picklist_names:  # Get a name from each of the extra good guys
        for m in range(0,len(ML[0])):  # Search through the master list of good guys.
            if ML[0][m].Name == extra_GG:  # if the name matches,
                random_BG_target = random.choice(BG_picklist_names)  # get a random bad guy name
                ML[0][m].Opponents.append(random_BG_target)  # and assign it to the extra good guy.
                # Now circle back to add these good guy opponents to the bad guys who are outnumbered.
                for n in range(0,len(ML[1])):  # Look through the master BG list
                    if ML[1][n].Name == random_BG_target:  # until you find the randomly-selected name
                        ML[1][n].Opponents.append(ML[0][m].Name) # add that GG to its opponent list.
                # BG_picklist_names.remove(random_BG_target) # so the BG has no more than 2 on 1

print()

# Now make a single list of combatants ranked from highest INIT to lowest.
CombatOrder = []
for goodguy in ML[0]:
    CombatOrder.append(goodguy)
for badguy in ML[1]:
    CombatOrder.append(badguy)

CombatOrder = sorted(CombatOrder, key=lambda hero: hero.INIT, reverse = True)

# CombatOrder contains copies of both good guy and bad guy instances from ML.  Keep the list current and ML does
# not need to be updated from here on out.

# At this point, all combatants have been defined and paired up. All of this is kept in the list CombatOrder which has been
# sorted by initiative.

# print list of attackers and all of their potential opponents (comment next line out if you don't want to see it every run)
ListCombatOrder(CombatOrder)



FightOver = False
Round = 1
AttackerCount = 1


print()

# refill picklist name lists before combat begins so they can be used for opponent reassignment when someone dies.
GG_picklist_names = []
for GG in ML[0]:
    GG_picklist_names.append(GG.Name)

BG_picklist_names = []
for BG in ML[1]:
    BG_picklist_names.append(BG.Name)

while not FightOver:
    AttackerCount = 1
    if verbose:
        print()
        print('Round {:3d}:'.format(Round))
    for attacker in CombatOrder:
        # if AttackerCount == 3: # just for testing
        #     attacker.hp = 0
        if verbose:
            print('   Attacker ' + str(AttackerCount))
        if attacker.hp < 1:  # attacker is dead, so go to the next attacker.
            if verbose:
                print("      {:8} cannot attack because they're dead.".format(attacker.Name))
            AttackerDead = True
        else:  # attacker is alive and can fight!
            AttackerDead = False
            # Set the defender
            if len(attacker.Opponents) > 1: # multiple opponents available, so pick one at random
                defender_name = random.choice(attacker.Opponents)
                if verbose:
                    print('      {:8} will fight {:8}, chosen at random.'.format(attacker.Name, defender_name))
            elif len(attacker.Opponents) == 1: # only one opponent
                defender_name = attacker.Opponents[0]
                if verbose:
                    print('      {:8} will fight {:8}'.format(attacker.Name, defender_name))
            else:
                if verbose:
                    print('      {:8} has an empty opponent list, so fill it with an available opponent.'.format(attacker.Name))
                if attacker.GoodSide:
                    if len(BG_picklist_names) > 0: # there are people to choose from
                        RandomNewTarget = random.choice(BG_picklist_names)
                        defender_name = RandomNewTarget
                        if verbose:
                            print('      {:8} now has {:8} as a new BG target.'.format(attacker.Name,defender_name))
                    else:
                        FightOver = True
                        if verbose:
                            print('      There are no more potential BG targets available.  Fight is over.')
                else:
                    if len(GG_picklist_names) > 0: # there are people to choose from
                        RandomNewTarget = random.choice(GG_picklist_names)
                        defender_name = RandomNewTarget
                        if verbose:
                            print('      {:8} now has {:8} as a new GG target.'.format(attacker.Name,defender_name))
                    else:
                        FightOver = True
                        if verbose:
                            print('      There are no more potential GG targets available.  Fight is over.')


                attacker.Opponents.append(defender_name)

            # Now resolve the combat between attacker and defender.
            for t in range(0,attacker.NumAT): # Loop through the number of attacks the attacker gets
                if verbose:
                    print('      Attack {:3d} - roll to hit:'.format(t+1))
                Base_attack_roll = dice(1,20)
                RL_combat_seconds += Die_Roll_Duration_secs
                AT_modifier = attacker.MeleeATmod
                Attack_roll = Base_attack_roll + AT_modifier
                if Attack_roll >= GetCombatantAC(defender_name): # It's a hit!
                    damage = dice(attacker.MeleeATdam_NumDice,attacker.MeleeATdam_DiceFaces) + attacker.MeleeATdam_Mod
                    RL_combat_seconds += (Die_Roll_Duration_secs * attacker.MeleeATdam_NumDice)
                    if verbose:
                        print('          ' + attacker.Name + ' hits ' + defender_name + ' for ' + str(damage) + ' pts of damage!')
                    # Get defender's HP value
                    DHP = getDefHP(defender_name)
                    if (DHP - damage) < 1:
                        if verbose:
                            print('          {:8} is DEAD.'.format(defender_name))
                        # Since they're dead, they aren't a viable opponent anymore, so take them out of all opponent lists
                        for zz in CombatOrder:
                            if defender_name in zz.Opponents:
                                zz.Opponents.remove(defender_name)
                                if verbose:
                                    print('          {:8} has been removed from the Opponents list of {:8}.'.format(defender_name, zz.Name))

                        # Since they're dead, they should also be removed from the list of potential opponents
                        if len(GG_picklist_names) > 0:
                            for a in GG_picklist_names:
                                if a == defender_name:
                                    GG_picklist_names.remove(defender_name)
                        # else:
                            # FightOver = True

                        if len(BG_picklist_names) > 0:
                            for b in BG_picklist_names:
                                if b == defender_name:
                                    BG_picklist_names.remove(defender_name)
                        # else:
                            # FightOver = True

                    # Finally, update the defender's hit point value as they just got tagged by the attacker!
                    UpdateCO_defender_hp(defender_name,damage)

                else:
                    damage = 0
                    if verbose:
                        print('          {:8} misses {:8}!  No damage taken.'.format(attacker.Name,defender_name))

        AttackerCount += 1

 #   print()
 #   x = 1
 #   for attacker in CombatOrder:
 #       print(x,attacker.INIT, attacker.Name, attacker.hp, attacker.Opponents)
 #       x += 1
 #   print()

#    print('GG_picklist_names = ', GG_picklist_names)
#    print('BG_picklist_names = ', BG_picklist_names)

    if len(GG_picklist_names) == 0:
        FightOver = True
        print('\n Game over! After {:3} rounds, all good guys have been defeated and there are {:3} bad guys remaining.'.format(Round,len(BG_picklist_names)))
        # print ('\nGame over!  After ' + str(Round) + ' rounds, all good guys have been defeated and the bad guys win.')
    if len(BG_picklist_names) == 0:
        FightOver = True
        # print ('\nGame over!  After ' + str(Round) + ' rounds, all bad guys have been defeated and the good guys win.')
        print('\n Game over! After {:3} rounds, all bad guys have been defeated and there are {:3} good guys remaining.'.format(Round,len(GG_picklist_names)))

    Round += 1
 
print('In game time, this battle took {:3d} seconds'.format(Round*6))
print()
print('Real life setup time:')
convert_time(RL_setup_seconds)
print()
print('Real life combat time:')
convert_time(RL_combat_seconds)

#print('In real life, it would have taken {:3d} seconds to set up this combat!'.format(RL_setup_seconds))
#print('In real life, it would have taken {:3d} seconds to play this out!'.format(RL_combat_seconds))



