import random
# import operator

def dice(numdice,faces):
	sum = 0
	for x in range(0,numdice):
		sum += random.randint(1,faces)
	return sum

def modifier(stat):
	if (stat > 9):
		modifier = int((stat/2) - 5)
	elif (stat == 8 or stat == 9):
		modifier = -1
	elif (stat == 6 or stat == 7):
		modifier = -2
	elif (stat == 4 or stat == 5):
		modifier = -3
	elif (stat == 2 or stat == 3):
		modifier = -4
	else:
		modifier = -5
	return modifier

GG_Classes = ['Guard','Knight']
BG_Classes = ['Goblin','Goblin Boss','Zombie','Orc','Bugbear','Bugbear Chief','Ogre']

class Combatant():  # this is the parent class from which is derived heroes and monsters
	def __init__(self):
		self.Name = 'Bob'
		self.STR = 10 # default value, to be modified per inherited class
		self.DEX = 10 # default value, to be modified per inherited class
		self.CON = 10 # default value, to be modified per inherited class
		self.INT = 10 # default value, to be modified per inherited class
		self.WIS = 10 # default value, to be modified per inherited class
		self.CHR = 10 # default value, to be modified per inherited class
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = self.STRmod
		self.MeleeATdam_DiceFaces = 8
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 2
		self.RangedATmod = self.DEXmod
		self.RangedATdam_DiceFaces = 6
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 10 # assume standard issue armor
		self.HitDiceNum = 1  # number of hit dice to roll
		self.HitDiceFaces = 6  # type of dice to use
		self.HitDiceMod = 2 # modifier to hit dice roll
		self.Opponents = [] # list of opponent names

class Goblin():
	def __init__(self):
		self.Name = 'Goblin_' # will be changed
		self.Label = 'Goblin_'
		self.STR = 8 # default value, to be modified per inherited class
		self.DEX = 14 # default value, to be modified per inherited class
		self.CON = 10 # default value, to be modified per inherited class
		self.INT = 10 # default value, to be modified per inherited class
		self.WIS = 8 # default value, to be modified per inherited class
		self.CHR = 8 # default value, to be modified per inherited class
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = 4
		self.MeleeATdam_DiceFaces = 6
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 2
		self.RangedATmod = 4
		self.RangedATdam_DiceFaces = 6
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 2
		self.AC = 15 # leather armor and a shield
		self.HitDiceNum = 2  # number of hit dice to roll
		self.HitDiceFaces = 6  # type of dice to use
		self.HitDiceMod = 0 # modifier to hit dice roll
		self.Opponents = [] # list of opponent names
		self.GoodSide = False
		self.BadSide = True

class GoblinBoss():
	def __init__(self):
		self.Name = 'GoblinBoss_' # will be changed
		self.Label = 'GoblinBoss_'
		self.STR = 10 # default value, to be modified per inherited class
		self.DEX = 14 # default value, to be modified per inherited class
		self.CON = 10 # default value, to be modified per inherited class
		self.INT = 10 # default value, to be modified per inherited class
		self.WIS = 8 # default value, to be modified per inherited class
		self.CHR = 10 # default value, to be modified per inherited class
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = 4
		self.MeleeATdam_DiceFaces = 6
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 2
		self.RangedATmod = 2
		self.RangedATdam_DiceFaces = 6
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 17 # leather armor and a shield
		self.HitDiceNum = 6  # number of hit dice to roll
		self.HitDiceFaces = 6  # type of dice to use
		self.HitDiceMod = 0 # modifier to hit dice roll
		self.Opponents = [] # list of opponent names
		self.GoodSide = False
		self.BadSide = True


class Zombie():
	def __init__(self):
		self.Name = 'Zombie_' # will be changed
		self.Label = 'Zombie_'
		self.STR = 13 # default value, to be modified per inherited class
		self.DEX = 6 # default value, to be modified per inherited class
		self.CON = 16 # default value, to be modified per inherited class
		self.INT = 3 # default value, to be modified per inherited class
		self.WIS = 6 # default value, to be modified per inherited class
		self.CHR = 5 # default value, to be modified per inherited class
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = 3
		self.MeleeATdam_DiceFaces = 6
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 1
		self.RangedATmod = 0
		self.RangedATdam_DiceFaces = 1
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 8 # leather armor and a shield
		self.HitDiceNum = 3  # number of hit dice to roll
		self.HitDiceFaces = 8  # type of dice to use
		self.HitDiceMod = 9 # modifier to hit dice roll
		self.Opponents = [] # list of opponent names
		self.GoodSide = False
		self.BadSide = True
class Skeleton():
	def __init__(self):
		self.Name = 'Skeleton_' # will be changed
		self.Label = 'Skeleton_'
		self.STR = 10 # default value, to be modified per inherited class
		self.DEX = 14 # default value, to be modified per inherited class
		self.CON = 15 # default value, to be modified per inherited class
		self.INT = 6 # default value, to be modified per inherited class
		self.WIS = 8 # default value, to be modified per inherited class
		self.CHR = 5 # default value, to be modified per inherited class
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = 4
		self.MeleeATdam_DiceFaces = 6
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 2
		self.RangedATmod = 4
		self.RangedATdam_DiceFaces = 6
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 2
		self.AC = 13 # armor scraps
		self.HitDiceNum = 2  # number of hit dice to roll
		self.HitDiceFaces = 8  # type of dice to use
		self.HitDiceMod = 4 # modifier to hit dice roll
		self.Opponents = [] # list of opponent names
		self.GoodSide = False
		self.BadSide = True

class Orc():
	def __init__(self):
		self.Name = 'Orc_' # will get changed
		self.Label = 'Orc_'
		self.STR = 16 # default value, to be modified per inherited class
		self.DEX = 12 # default value, to be modified per inherited class
		self.CON = 16 # default value, to be modified per inherited class
		self.INT = 7 # default value, to be modified per inherited class
		self.WIS = 11 # default value, to be modified per inherited class
		self.CHR = 10 # default value, to be modified per inherited class
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = 5
		self.MeleeATdam_DiceFaces = 12
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 3
		self.RangedATmod = 0
		self.RangedATdam_DiceFaces = 1
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 13 # hide armor
		self.HitDiceNum = 2  # number of hit dice to roll
		self.HitDiceFaces = 8  # type of dice to use
		self.HitDiceMod = 6 # modifier to hit dice roll
		self.Opponents = [] # list of opponent names
		self.GoodSide = False
		self.BadSide = True

class Bugbear():
	def __init__(self):
		self.Name = 'Bugbear_'
		self.Label = 'Bugbear_'
		self.STR = 15 # default value, to be modified per inherited class
		self.DEX = 14 # default value, to be modified per inherited class
		self.CON = 13 # default value, to be modified per inherited class
		self.INT = 8 # default value, to be modified per inherited class
		self.WIS = 11 # default value, to be modified per inherited class
		self.CHR = 9 # default value, to be modified per inherited class
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = 4
		self.MeleeATdam_DiceFaces = 8
		self.MeleeATdam_NumDice = 2
		self.MeleeATdam_Mod = 2
		self.RangedATmod = 0
		self.RangedATdam_DiceFaces = 1
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 16 # hide armor and shield
		self.HitDiceNum = 5  # number of hit dice to roll
		self.HitDiceFaces = 8  # type of dice to use
		self.HitDiceMod = 5 # modifier to hit dice roll
		self.Opponents = [] # list of opponent names
		self.GoodSide = False
		self.BadSide = True

class BugbearChief():
	def __init__(self):
		self.Name = 'BugbearChief_'  # will get changed
		self.Label = 'BugbearChief_'
		self.STR = 17 # default value, to be modified per inherited class
		self.DEX = 14 # default value, to be modified per inherited class
		self.CON = 14 # default value, to be modified per inherited class
		self.INT = 11 # default value, to be modified per inherited class
		self.WIS = 12 # default value, to be modified per inherited class
		self.CHR = 11 # default value, to be modified per inherited class
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 2
		self.MeleeATmod = 5
		self.MeleeATdam_DiceFaces = 8
		self.MeleeATdam_NumDice = 2
		self.MeleeATdam_Mod = 3
		self.RangedATmod = 0
		self.RangedATdam_DiceFaces = 1
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 17 # hide armor and shield
		self.HitDiceNum = 10  # number of hit dice to roll
		self.HitDiceFaces = 8  # type of dice to use
		self.HitDiceMod = 20 # modifier to hit dice roll
		self.Opponents = [] # list of opponent names
		self.GoodSide = False
		self.BadSide = True

class Ogre():
	def __init__(self):
		self.Name = 'Ogre_'
		self.Label = 'Ogre_'
		self.STR = 19 # default value, to be modified per inherited class
		self.DEX = 8 # default value, to be modified per inherited class
		self.CON = 16 # default value, to be modified per inherited class
		self.INT = 5 # default value, to be modified per inherited class
		self.WIS = 7 # default value, to be modified per inherited class
		self.CHR = 7 # default value, to be modified per inherited class
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = 6
		self.MeleeATdam_DiceFaces = 8
		self.MeleeATdam_NumDice = 2
		self.MeleeATdam_Mod = 4
		self.RangedATmod = 0
		self.RangedATdam_DiceFaces = 1
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 11 # hide armor
		self.HitDiceNum = 7  # number of hit dice to roll
		self.HitDiceFaces = 10  # type of dice to use
		self.HitDiceMod = 21 # modifier to hit dice roll
		self.Opponents = [] # list of opponent names
		self.GoodSide = False
		self.BadSide = True

class Guard(Combatant):
	def __init__(self):
		# Override certain values from parent class
		self.Name = 'Guard_'
		self.Label = 'Guard_'
		self.STR = 13
		self.DEX = 12
		self.CON = 12
		self.INT = 10
		self.WIS = 11
		self.CHR = 10
		# self.INIT = 1 # default value, to be updated at instantiation
		# self.hp = 1   # default value, to be updated at instantiation
		# update mod values based on stats
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = self.STRmod
		self.MeleeATdam_DiceFaces = 8
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 2
		self.RangedATmod = self.DEXmod
		self.RangedATdam_DiceFaces = 6
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 16 # assume chainmail shirt and shield
		self.HitDiceNum = 1  # number of hit dice to roll
		self.HitDiceFaces = 6  # type of dice to use
		self.HitDiceMod = 1 # modifier to hit dice roll
		self.Opponents = []
		self.GoodSide = True
		self.BadSide = False

class Knight(Combatant):
	def __init__(self):
		# Override certain values from parent class
		self.Name = 'Knight_'
		self.Label = 'Knight_'
		self.STR = 16
		self.DEX = 11
		self.CON = 14
		self.INT = 11
		self.WIS = 11
		self.CHR = 15
		# self.INIT = 1 # default value, to be updated at instantiation
		# self.hp = 1   # default value, to be updated at instantiation
		# update mod values based on stats
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 2
		self.MeleeATmod = 5
		self.MeleeATdam_DiceFaces = 6
		self.MeleeATdam_NumDice = 2
		self.MeleeATdam_Mod = 3
		self.RangedATmod = self.DEXmod
		self.RangedATdam_DiceFaces = 6
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 18 # plate armor
		self.HitDiceNum = 8  # number of hit dice to roll
		self.HitDiceFaces = 8  # type of dice to use
		self.HitDiceMod = 16 # modifier to hit dice roll
		self.Opponents = []
		self.GoodSide = True
		self.BadSide = False




class Bandit():
	def __init__(self):
		self.Name = 'Bandit_'
		self.Label = 'Bandit_'
		self.STR = 11
		self.DEX = 12
		self.CON = 12
		self.INT = 10
		self.WIS = 10
		self.CHR = 10
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = self.STRmod
		self.MeleeATdam_DiceFaces = 6
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 2
		self.RangedATmod = self.DEXmod
		self.RangedATdam_DiceFaces = 6
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		self.AC = 14 # Assume leather armor
		self.HitDiceNum = 1  # number of hit dice to roll
		self.HitDiceFaces = 6  # type of dice to use
		self.HitDiceMod = 2 # modifier to hit dice roll
		self.Opponents = []
		self.GoodSide = False
		self.BadSide = True

class FireNewt():
	def __init__(self):
		self.Name = 'FireNewt_'
		self.Label = 'FireNewt_'
		self.STR = 10
		self.DEX = 13
		self.CON = 12
		self.INT = 7
		self.WIS = 11
		self.CHR = 8
		self.INIT = 1 # default value, to be updated at instantiation
		self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 2
		self.MeleeATmod = self.STRmod
		self.MeleeATdam_DiceFaces = 6
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 1
		self.RangedATmod = self.DEXmod
		self.RangedATdam_DiceFaces = 8
		self.RangedATdam_NumDice = 2
		self.RangedATdam_Mod = 0
		self.AC = 16 # Assume leather armor
		self.HitDiceNum = 4  # number of hit dice to roll
		self.HitDiceFaces = 8  # type of dice to use
		self.HitDiceMod = 4 # modifier to hit dice roll
		self.Opponents = []
		self.GoodSide = False
		self.BadSide = True


class PC():
	def __init__(self,Name,STR,DEX,CON,INT,WIS,CHR,HP,AC):
		self.Name = Name
		self.Label = Name
		self.STR = STR
		self.DEX = DEX
		self.CON = CON
		self.INT = INT
		self.WIS = WIS
		self.CHR = CHR
		self.hp = HP
		self.AC = AC
		self.INIT = 1 # default value, to be updated at instantiation
		#self.hp = 1   # default value, to be updated at instantiation
		self.STRmod = modifier(self.STR)
		self.DEXmod = modifier(self.DEX)
		self.CONmod = modifier(self.CON)
		self.INTmod = modifier(self.INT)
		self.WISmod = modifier(self.WIS)
		self.CHRmod = modifier(self.CHR)
		self.NumAT = 1
		self.MeleeATmod = self.STRmod
		self.MeleeATdam_DiceFaces = 8
		self.MeleeATdam_NumDice = 1
		self.MeleeATdam_Mod = 2
		self.RangedATmod = self.DEXmod
		self.RangedATdam_DiceFaces = 6
		self.RangedATdam_NumDice = 1
		self.RangedATdam_Mod = 0
		#self.AC = 15 # Assume studded armor for heroes
		self.HitDiceNum = 1  # number of hit dice to roll
		self.HitDiceFaces = 6  # type of dice to use
		self.HitDiceMod = 2 # modifier to hit dice roll
		self.GoodSide = True
		self.BadSide = False
		self.Opponents = []


	def __str__(self):
		message = "My name is " + str(self.Name) + "\n"
		message += "My STR = " + str(self.STR) + "(" + str(self.STRmod) + ")\n"
		message += "My DEX = " + str(self.DEX) + "(" + str(self.DEXmod) + ")\n"
		message += "My CON = " + str(self.CON) + "(" + str(self.CONmod) + ")\n"
		message += "My INT = " + str(self.INT) + "(" + str(self.INTmod) + ")\n"
		message += "My WIS = " + str(self.WIS) + "(" + str(self.WISmod) + ")\n"
		message += "My CHR = " + str(self.CHR) + "(" + str(self.CHRmod) + ")\n"
		message += "My INIT = " + str(self.INIT)
		return(message)




def Tavern():
	# This generates a random Tavern type and name
	TavType = [
		'quiet, low-key bar','raucous dive','thieves guild hangout',
		'gathering place for a secret society','upper-class dining club',
		'gambling den','snooty hangout for Elven wizards','members-only club',
		'brothel','popular music venue','meeting place for a local book group']

	TavNameFirst = [
		'The Silver ',
		'The Golden ',
		'The Staggering ',
		'The Laughing ',
		'The Prancing ',
		'The Gilded ',
		'The Running ',
		'The Howling ',
		'The Gassy ',
		'The Leering ',
		'The Drunken ',
		'The Leaping ',
		'The Roaring ',
		'The Frowning ',
		'The Lonely ',
		'The Wandering ',
		'The Mysterious ',
		'The Barking ',
		'The Black ',
		'The Gleaming '
	]

	TavNameSecond = [
		'Eel',
		'Dolphin',
		'Dwarf',
		'Pegasus',
		'Pony',
		'Rose',
		'Stag',
		'Wolf',
		'Lamb',
		'Demon',
		'Goat',
		'Spirit',
		'Gryphon',
		'Jester',
		'Mountain',
		'Eagle',
		'Satyr',
		'Dog',
		'Knight',
		'Star',
	]
	msg = "You see a tavern called "
	msg += TavNameFirst[dice(1,len(TavNameFirst))-1]
	msg += TavNameSecond[dice(1,len(TavNameSecond))-1]
	msg += " known for being a "
	msg += TavType[dice(1,len(TavType))-1]
	print(msg)

	# msg += TavNameFirst[dice(1,len(TavNameFirst))-1]
	# msg += TavNameSecond[dice(1,len(TavNameSecond))-1]
	# msg += " known for being a "
	# msg += TavType[dice(1,len(TavType))-1]
	# print(msg)








def Goals():
	GoalsDungeon = [
		"stop the dungeeon's monsterous inhabitants from raiding the surface world.",
		"foil a villan's evil scheme",
		"destroy a magical threat inside the dungeon",
		"acquire treasure",
		"find a particular item for a specific purpose",
		"retrieve a stolen item hidden in the dungeon",
		"find information needed for a special purpose",
		"rescue a captive",
		"discover the fate of a previous adventuring party",
		"find an NPC who disappeared in the area",
		"slay a dragon or some other challenging monster",
		"discover the nature and origin of a strange location or phenomena",
		"pursue fleeing foes taking refuge in the dungeon",
		"escape from captivity in the dungeon",
		"clear a ruin so it can be rebuilt and reoccupied",
		"discover why a villan is interested in the dungeon",
		"win a bet or complete a rite of passage by surviving in the dungeon for a certain amount of time",
		"parley with a villan in the dungeon",
		"hide from a threat outside the dungeon",
		"find a secret dungeon, kill everything, loot it then destroy it."]

	GoalsWilderness = [
		"locate a dungeon or other site of interest ",
		"assess the scope of a natural or unnatural disaster."
		"escort an NPC to a destination.",
		"arrive at a destination without being seen by the villan's forces.",
		"stop monsters from raiding caravans and farms.",
		"establish trade with a distant town.",
		"protect a caravan travelling to a distant town.",
		"map a new land.",
		"find a place to establish a colony.",
		"find a natural resource.",
		"hunt a specific monster.",
		"return home from a distant place.",
		"obtain information from a reclusive hermit.",
		"find an object that was lost in the wilds.",
		"discover the fate of a missing group of explorers.",
		"pursue fleeing foes.",
		"assess the size of an approaching army.",
		"escape the reign of a tyrant.",
		"protect a wilderness site from attackers.",
		"foil the plans of a tyrant to destroy a wild area."]

	msg = "The goal of this adventure is to "
	
	D_or_W = dice(1,2) # roll for either dungeon or wilderness goal
	if(D_or_W == 1):  # Dungeon goal
		choice = random.randint(0,len(GoalsDungeon))
		msg += GoalsDungeon[choice]
	else: # wilderness goal
		choice = random.randint(0,len(GoalsWilderness))
		if choice == 0:
			Dchoice = random.randint(0,len(GoalsDungeon))
			msg += GoalsWilderness[0] + 'in order to ' + GoalsDungeon[Dchoice]
		else:
			msg += GoalsWilderness[choice]
	print(msg)
	
	
