# List of numbers (python has nothing they call an 'array' as a type)
List1 = [23,65,42,101,420]
print(List1[2])   # will print 42
print('Hello')

# List of strings
List2 = ['Kanku','Gojushijo','Henka 4']

# List of mixed types
List3 = ['abc', 12, 'Thor', 94022]

# List of lists
List4 = [List1, List2, List3]
print(List4[1][2])  # will print 'Sandan'

# Cool list operations
List5 = [3,8,2,4,1,9,7,6,5]
print(List5) 
List5.sort(key=None,reverse=True)
print(List5)
List5.append(10)
print(List5)

# dynamically build a list
A = []  # empty list
for i in range(0,20):
    A.append(i)

print(A)