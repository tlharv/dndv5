from DNDv5 import *

# This program resolves combat between large numbers of opponents.  It roughly follows
#   the rules of D&D 5e but some changes are needed to make it somewhat realistic.
#   At the beginning of combat, opponent pairs are generated based on everyone's initiative
#   rolls.  Then during each round, each opponent pair takes turns.  


def PrintCombatPairs(combatpair):
	for i in range(0,len(combatpair)):
		print(combatpair[i][0].Name, combatpair[i][1].Name)


def resolve_opponent_pair(OP1, OP2):
    # This function assumes that opponent OP1 is attacking opponent OP2
    print(OP1.Name + " attacks " + OP2.Name)
    AttackRoll = dice(1,20) + OP1.MeleeATmod
    if (AttackRoll > OP2.AC):
        Damage = dice(OP1.MeleeATdam_NumDice,OP1.MeleeATdam_DiceFaces) + OP1.MeleeATdam_Mod
        print(OP1.Name + " hits " + OP2.Name + " for " + str(Damage) + " points of damage!")
    else:
        print(OP1.Name + " misses!")
        Damage = 0
    return Damage

Tavern()

goodguys = int(input("How many good guys? "))
badguys = int(input("How many bad guys? "))

GG = []      # list of good guy instances of hero class
BG = []      # list of bad guy instances of hero class

print()

# Define the good guys
for x in range(0,goodguys):
    name = "Guard_" + str(x)
    dude = Guard(name)
    dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
    dude.hp = (dice(2,8) + 2) # roll individual hp
    GG.append(dude)

# sort the list of good guys
newGG = sorted(GG, key=lambda hero: hero.INIT, reverse = True)


# Define the bad guys
for y in range(0,badguys):
    name = "Bandit_" + str(y)
    dude = Bandit(name)
    dude.INIT = dice(1,20) + modifier(dude.DEX) # roll new INIT
    dude.hp = (dice(2,8) + 2) # roll individual hp
    BG.append(dude)

# sort the list of bad guys
newBG = sorted(BG, key=lambda hero: hero.INIT, reverse = True)

combatpair = []
# Now create the list of combat order.
if (len(newGG) == len(newBG)): # equal numbers of GG and BG
    # print('Same number of good guys and bad guys')
    for i in range(0,len(newGG)):
        combatpair.append([newGG[i],newBG[i]]) # create matchup

elif (len(newGG) > len(newBG)): # GG outnumber BG
    # print('Good guys outnumber bad guys') # remaining good guys will stand idle
    for i in range(0,len(newBG)): # combat pairs limited by side with least numbers
        combatpair.append([newGG[i],newBG[i]]) # create matchup
    idle = len(newGG) - len(newBG)
    print(str(idle) + ' good guys will stand idle and watch the fight')

else:
    # print('Bad guys outnumber good guys') # remaining bad guys will stand idle
    for i in range(0,len(newGG)): # combat pairs limited by side with least numbers
        combatpair.append([newGG[i],newBG[i]]) # create matchup
    idle = len(newBG) - len(newGG)
    print(str(idle) + ' bad guys will stand idle and watch the fight')

print("Combat will involve " + str(len(combatpair)) + " pairs.")

print()

# print the combat pairs

PrintCombatPairs(combatpair)
print()

combatpair.pop()
PrintCombatPairs(combatpair)
print()

combatpair.remove(combatpair[1])
PrintCombatPairs(combatpair)
