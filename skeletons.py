from DND5e import *

def ListCombatOrder(CombatOrder):
	#print('There are {:3d} combatants.'.format(len(CombatOrder)))
	print()
	print('INIT	Combatant        AC       HP')
	print('------------------------------------')
	for attacker in CombatOrder:
		print('{:3d}	{:12}    {:3d}      {:3d}'.format(attacker.INIT,attacker.Name,attacker.AC,attacker.hp))
	print()


Num_BG = int(input('Enter number of skeletons: '))

BG = []
CombatOrder = []

# define the good guys
Marlock = PC('Marlock',10,15,14,16,15,16,22,12)
Marlock.INIT = int(input('INIT for Marlock & Bimms: '))

Z = PC('Z',18,15,16,11,10,10,55,15)
Z.INIT = int(input('INIT for Z: '))

Vienna = PC('Vienna/Patty',13,16,14,13,16,10,44,16)
Vienna.INIT = int(input('INIT for Vienna & Patty: '))

Ren = PC('Ren',13,18,16,11,13,10,43,15)
Ren.INIT = int(input('INIT for Ren: '))


# define the bad guys
for x in range (0,Num_BG):
	name = "Skeleton_" + str(x+1)
	dude = Skeleton()
	dude.Name = name
	dude.INIT = dice(1,20) + modifier(dude.DEX)
	dude.hp = 13
	BG.append(dude)

# Add all combatants to one big list
CombatOrder.append(Marlock)
CombatOrder.append(Z)
CombatOrder.append(Vienna)
CombatOrder.append(Ren)
for skeleton in BG:
	CombatOrder.append(skeleton)

# Sort the list of combatants by INIT
CombatOrder = sorted(CombatOrder, key=lambda hero: hero.INIT, reverse = True)

# print out all combatants in order of INIT
ListCombatOrder(CombatOrder)


