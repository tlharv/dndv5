from DNDv5 import *

goodguys = int(input("How many good guys? "))
badguys = int(input("How many bad guys? "))

GG = []      # list of good guy instances of hero class
BG = []      # list of bad guy instances of hero class
GG_name = [] # list of just the good guy names
BG_name = [] # list of just the bad guy names
GG_init = [] # list of just the good guy initiative rolls
BG_init = [] # list of just the bad guy initiative rolls
d = {}       # dictionary of all good guy & bad guy key value pairs; {name:initiative}; to be sorted by initiative



for x in range(0,goodguys):
    name = "GG_" + str(x)
    GG_name.append(name)
    GG[x] = hero(name,10,10,10,10,10,10)
    init = dice(1,20) + GG[x].initbonus
    GG_init.append(init)

for y in range(0,badguys):
    name = "BG_" + str(y)
    BG_name.append(name)
    BG[y] = hero(name,10,10,10,10,10,10)
    init = dice(1,20) + BG[y].initbonus
    BG_init.append(init)


# print(GG_name)
# print(GG_init)

d = {}
for i in range(0,goodguys):
    d.update({GG_name[i]:GG_init[i]})

for j in range(0,badguys):
    d.update({BG_name[j]:BG_init[j]})

# print("unsorted:")
# print(d)
# print()
# print("sorted:")
s = SortInitDictionary(d)

for j in range(0,len(s)):
    print(s[j])


print(BG_1)
