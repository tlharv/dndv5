import random
import time

def d100():
    return random.randint(1,100)

def d6(dice):
    sum = 0
    for i in range(1,dice+1):
        sum += random.randint(1,6)
    return sum

def treasure_0_4():
    roll = d100()
    cp = 0
    sp = 0
    ep = 0
    gp = 0
    pp = 0
    if (roll <= 30):
        cp += d6(5)
    if (roll >= 31 and roll < 61):
        sp += d6(4)
    if (roll >= 61 and roll < 71):
        ep += d6(3)
    if (roll >= 71 and roll < 96):
        gp += d6(3)
    if (roll >= 96 and roll < 101):
        pp += d6(1)
    loot = [cp, sp, ep, gp, pp]
    return loot

def convert_time(time):
    day = time // (24 * 3600)
    time = time % (24 * 3600)
    hour = time // 3600
    time %= 3600
    minutes = time // 60
    time %= 60
    seconds = time
    print("d:h:m:s-> %d:%d:%d:%d" % (day, hour, minutes, seconds))

def maximize_gp(purse):
	# purse is the list of cp, sp, ep, gp, and pp
	# function will convert as much of this list to gp and minimize the rest
	# 100 cp to 1 gp
	# 10 sp to 1 gp
	# 2 ep to 1 gp
	# 10 gp to 1 pp
	#for coin in purse:
	cp = purse[0]
	sp = purse[1]
	ep = purse[2]
	gp = purse[3]
	pp = purse[4]

	# pp is easy, so do that first
	gp += pp * 10
	pp = 0
	# now convert cp, sp and ep to cp (lowest common denominator)
	# 50 cp to an ep
	cp += ep * 50
	ep = 0
	cp += sp * 10
	sp = 0
	cp_left = cp % 100
	cp -= cp_left
	gp += cp / 100
	cp = cp_left
	print("You now have " + str(gp) + " gp with " + str(cp_left) + " cp left over.")
	newpurse = [cp, sp, ep, gp, pp]
#	return newpurse

# Main part of the program
badguys = int(input("How many bad guys were there (CR 0-4)? "))
totalcp = 0
totalsp = 0
totalep = 0
totalgp = 0
totalpp = 0

for x in range(1,badguys+1):
    my_loot = treasure_0_4() # returns list [cp, sp, ep, gp, pp] per bad guy
    totalcp += my_loot[0]
    totalsp += my_loot[1]
    totalep += my_loot[2]
    totalgp += my_loot[3]
    totalpp += my_loot[4]

print("Total loot:")
haul = [totalcp, totalsp, totalep, totalgp, totalpp]
print(str(totalcp) + " cp")
print(str(totalsp) + " sp")
print(str(totalep) + " ep")
print(str(totalgp) + " gp")
print(str(totalpp) + " pp")
print()

maximize_gp(haul)
#print()


t = time.process_time()
print("Time elapsed: " + str(t) + " seconds")
convert_time(t)
print()
