from DNDv5 import *

class student:
    def __init__(self,name,grade,age):
        self.name = name
        self.grade = grade
        self.age = age



student_objects = [
    student('Isaac', 'A', 12),
    student('Nathan', 'A', 10),
    student('Marcus', 'A', 13),
    student('James', 'B', 11)
]

# sort by age
new = sorted(student_objects, key=lambda student: student.age, reverse = True)

for z in range(0,len(student_objects)):
    print(new[z].age, new[z].name, new[z].grade)

goodguys = int(input("How many good guys? "))
GG = []      # list of good guy instances of hero class
for x in range(0,goodguys):
    name = "GG_" + str(x)
    dude = hero(name,10,10,10,10,10,10,1)
    init = dice(1,20) + modifier(dude.DEX)
    dude.INIT = init
    GG.append(dude)

newGG = sorted(GG, key=lambda hero: hero.INIT, reverse = True)

for x in range(0,len(newGG)):
    print(newGG[x].INIT, newGG[x].Name)