from operator import itemgetter
import random

# ------ DREWCIFER'S HANDY DANDY PYTHON INITIATIVE CALCULATOR FOR D&D -------
# Designed so you enter in your PCs' names once at the start of the night.
# Those names are re-used for each subsequent fight. Monsters are numbered
# simply, this should be tracked by the DM (put numbered tokens next to each
# monster or something to make this easy). At the end of each fight, hit enter
# to start over or 'n' to quit. If you need to add a new PC or make a mistake
# or something, just quit the program (CTRL + C) and run the script again to
# start over.

# Player setup
players = input('How many PCs? ')
player_names = []
for i in range(0, players):
    name = raw_input('PC ' + str(i + 1) + ' name: ')
    player_names.append(name)

# New encounter
new_fight = True

while new_fight is True:

    # Type in each player's initiative total
    player_init = []
    for i in range(0, players):
        pinit = input(player_names[i] + ' initiative total: ')
        player_init.append(pinit)

    # Combines the PC names with their initiative rolls in pairs in a list
    player_list = [None] * players
    for i in range(0, players):
        player_list[i] = ('- ' + player_names[i]), player_init[i]

    print(player_list)

    # How many enemies do we have?
    enemies = input('How many Monsters? ')

    # Type in each enemy's initiative modifier
    enemy_init = []
    for i in range(0, enemies):
        einit = input('Monster ' + str(i + 1) + ' initiative modifier? ')
        enemy_init.append(einit)

    # Rolls each enemy's initiative, adding their modifier, adds these rolls
    # into a list with the enemy names, in pairs
    monster_list = []
    for i in range(0, enemies):
        init_roll = random.randint(1, (20 + enemy_init[i]))
        monster_list.append(['- Monster ' + str(i + 1), init_roll])

    # Combines the list and sorts by initiative roll
    init_list = player_list + monster_list
    init_list = sorted(init_list, key=itemgetter(1), reverse=True)

    print("\n")
    print("--- Initiative list ---")

    # Prints the list in readable strings without brackets/commas etc
    for entry in init_list:
        print str(entry).strip("[]()").replace("'", "").replace(",", ":")

    # Choice to start a new battle with the same group of PCs, or quit
    print("\n")
    again = raw_input('Hit the enter key to start a new battle, or "n" to exit...')
    if again == "n":
        new_fight = False