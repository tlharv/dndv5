#!/usr/bin/python
import random

def d20():
    roll = random.randint(1,20)
    return roll

num_of_goblins = int(input('How many goblins? '))
num_of_bugbears = int(input('How many bugbears? '))
num_of_ogres = int(input('How many ogres? '))

goblin_inits = []
bugbear_inits = []
ogre_inits = []

for g in range(1,num_of_goblins+1):
    goblin_inits.append(d20()+4)

for b in range(1,num_of_bugbears+1):
     bugbear_inits.append(d20()+2)

for o in range(1,num_of_ogres+1):
    ogre_inits.append(d20()-1)

print(goblin_inits)
print(bugbear_inits)
print(ogre_inits)