from DND5e import *

# The global lists we'll use:
ML = []  # this is the MASTER LIST, the base reference for all combatants.
GG = []
BG = []
GG_picklist = []  # this is the list of good guys, names only.  Used for choosing opponents.
BG_picklist = []  # this is the list of good guys, names only.  Used for choosing opponents.

d20_rolls = 0
d12_rolls = 0
d10_rolls = 0
d8_rolls = 0
d6_rolls = 0
d4_rolls = 0
d2_rolls = 0

Verbose = False

# DEFINE THE ARMY
GG_army = [
    ['Guard',  5],
    ['Knight', 0]
]
BG_army = [
    ['Goblin',       10],
    ['Goblin Boss',   0],
    ['Bugbear',       0],
    ['Bugbear Chief', 0],
    ['Orc',           0],
    ['Ogre',          0],
    ['Zombie',        0]
]

# GG_Classes = ['Guard','Knight']
# BG_Classes = ['Goblin','Goblin Boss','Zombie','Orc','Bugbear','Bugbear Chief','Ogre']

def SetUpG_List(squad):  # This will update the global GG[] list with whatever squad you give it.
    global GG
    global BG
    global d20_rolls
    global d12_rolls
    global d10_rolls
    global d8_rolls
    global d6_rolls
    global d4_rolls

    # squad comes in as a list [class name, number of instances to build]
    ClassName = squad[0]
    NumInstances = squad[1]
    if ClassName == 'Guard':
        for x in range(0,NumInstances):
            dude = Guard()
            dude.Name = dude.Label + str(x)
            dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
            d20_rolls += 1
            dude.hp = (dice(dude.HitDiceNum,dude.HitDiceFaces) + dude.HitDiceMod)
            if dude.HitDiceFaces == 4:
                for i in range(0,dude.HitDiceNum):
                    d4_rolls += 1
            if dude.HitDiceFaces == 6:
                for i in range(0,dude.HitDiceNum):
                    d6_rolls += 1
            if dude.HitDiceFaces == 10:
                for i in range(0,dude.HitDiceNum):
                    d10_rolls += 1
            if dude.HitDiceFaces == 12:
                for i in range(0,dude.HitDiceNum):
                    d12_rolls += 1
            GG.append(dude) # unsorted list of all good guy instances
    elif ClassName == 'Knight':
        for y in range(0,NumInstances):
            dude = Knight()
            dude.Name = dude.Label + str(y)
            dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
            d20_rolls += 1
            dude.hp = (dice(dude.HitDiceNum,dude.HitDiceFaces) + dude.HitDiceMod)
            if dude.HitDiceFaces == 4:
                for i in range(0,dude.HitDiceNum):
                    d4_rolls += 1
            if dude.HitDiceFaces == 6:
                for i in range(0,dude.HitDiceNum):
                    d6_rolls += 1
            if dude.HitDiceFaces == 10:
                for i in range(0,dude.HitDiceNum):
                    d10_rolls += 1
            if dude.HitDiceFaces == 12:
                for i in range(0,dude.HitDiceNum):
                    d12_rolls += 1
            GG.append(dude) # unsorted list of all good guy instances
    elif ClassName == 'Goblin':
        for y in range(0,NumInstances):
            dude = Goblin()
            dude.Name = dude.Label + str(y)
            dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
            d20_rolls += 1
            dude.hp = (dice(dude.HitDiceNum,dude.HitDiceFaces) + dude.HitDiceMod)
            if dude.HitDiceFaces == 4:
                for i in range(0,dude.HitDiceNum):
                    d4_rolls += 1
            if dude.HitDiceFaces == 6:
                for i in range(0,dude.HitDiceNum):
                    d6_rolls += 1
            if dude.HitDiceFaces == 10:
                for i in range(0,dude.HitDiceNum):
                    d10_rolls += 1
            if dude.HitDiceFaces == 12:
                for i in range(0,dude.HitDiceNum):
                    d12_rolls += 1
            BG.append(dude) # unsorted list of all good guy instances
    elif ClassName == 'Goblin Boss':
        for y in range(0,NumInstances):
            dude = GoblinBoss()
            dude.Name = dude.Label + str(y)
            dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
            d20_rolls += 1
            dude.hp = (dice(dude.HitDiceNum,dude.HitDiceFaces) + dude.HitDiceMod)
            if dude.HitDiceFaces == 4:
                for i in range(0,dude.HitDiceNum):
                    d4_rolls += 1
            if dude.HitDiceFaces == 6:
                for i in range(0,dude.HitDiceNum):
                    d6_rolls += 1
            if dude.HitDiceFaces == 10:
                for i in range(0,dude.HitDiceNum):
                    d10_rolls += 1
            if dude.HitDiceFaces == 12:
                for i in range(0,dude.HitDiceNum):
                    d12_rolls += 1
            BG.append(dude) # unsorted list of all good guy instances
    elif ClassName == 'Bugbear':
        for y in range(0,NumInstances):
            dude = Bugbear()
            dude.Name = dude.Label + str(y)
            dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
            d20_rolls += 1
            dude.hp = (dice(dude.HitDiceNum,dude.HitDiceFaces) + dude.HitDiceMod)
            if dude.HitDiceFaces == 4:
                for i in range(0,dude.HitDiceNum):
                    d4_rolls += 1
            if dude.HitDiceFaces == 6:
                for i in range(0,dude.HitDiceNum):
                    d6_rolls += 1
            if dude.HitDiceFaces == 10:
                for i in range(0,dude.HitDiceNum):
                    d10_rolls += 1
            if dude.HitDiceFaces == 12:
                for i in range(0,dude.HitDiceNum):
                    d12_rolls += 1
            BG.append(dude) # unsorted list of all good guy instances
    elif ClassName == 'Bugbear Chief':
        for y in range(0,NumInstances):
            dude = BugbearChief()
            dude.Name = dude.Label + str(y)
            dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
            d20_rolls += 1
            dude.hp = (dice(dude.HitDiceNum,dude.HitDiceFaces) + dude.HitDiceMod)
            if dude.HitDiceFaces == 4:
                for i in range(0,dude.HitDiceNum):
                    d4_rolls += 1
            if dude.HitDiceFaces == 6:
                for i in range(0,dude.HitDiceNum):
                    d6_rolls += 1
            if dude.HitDiceFaces == 10:
                for i in range(0,dude.HitDiceNum):
                    d10_rolls += 1
            if dude.HitDiceFaces == 12:
                for i in range(0,dude.HitDiceNum):
                    d12_rolls += 1
            BG.append(dude) # unsorted list of all good guy instances
    elif ClassName == 'Orc':
        for y in range(0,NumInstances):
            dude = Orc()
            dude.Name = dude.Label + str(y)
            dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
            d20_rolls += 1
            dude.hp = (dice(dude.HitDiceNum,dude.HitDiceFaces) + dude.HitDiceMod)
            if dude.HitDiceFaces == 4:
                for i in range(0,dude.HitDiceNum):
                    d4_rolls += 1
            if dude.HitDiceFaces == 6:
                for i in range(0,dude.HitDiceNum):
                    d6_rolls += 1
            if dude.HitDiceFaces == 10:
                for i in range(0,dude.HitDiceNum):
                    d10_rolls += 1
            if dude.HitDiceFaces == 12:
                for i in range(0,dude.HitDiceNum):
                    d12_rolls += 1
            BG.append(dude) # unsorted list of all good guy instances
    elif ClassName == 'Ogre':
        for y in range(0,NumInstances):
            dude = Ogre()
            dude.Name = dude.Label + str(y)
            dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
            d20_rolls += 1
            dude.hp = (dice(dude.HitDiceNum,dude.HitDiceFaces) + dude.HitDiceMod)
            if dude.HitDiceFaces == 4:
                for i in range(0,dude.HitDiceNum):
                    d4_rolls += 1
            if dude.HitDiceFaces == 6:
                for i in range(0,dude.HitDiceNum):
                    d6_rolls += 1
            if dude.HitDiceFaces == 10:
                for i in range(0,dude.HitDiceNum):
                    d10_rolls += 1
            if dude.HitDiceFaces == 12:
                for i in range(0,dude.HitDiceNum):
                    d12_rolls += 1
            BG.append(dude) # unsorted list of all good guy instances
    elif ClassName == 'Zombie':
        for y in range(0,NumInstances):
            dude = Zombie()
            dude.Name = dude.Label + str(y)
            dude.INIT = (dice(1,20) + modifier(dude.DEX)) # roll new INIT
            d20_rolls += 1
            dude.hp = (dice(dude.HitDiceNum,dude.HitDiceFaces) + dude.HitDiceMod)
            if dude.HitDiceFaces == 4:
                for i in range(0,dude.HitDiceNum):
                    d4_rolls += 1
            if dude.HitDiceFaces == 6:
                for i in range(0,dude.HitDiceNum):
                    d6_rolls += 1
            if dude.HitDiceFaces == 10:
                for i in range(0,dude.HitDiceNum):
                    d10_rolls += 1
            if dude.HitDiceFaces == 12:
                for i in range(0,dude.HitDiceNum):
                    d12_rolls += 1
            BG.append(dude) # unsorted list of all good guy instances

def printcombatpairs(CL):
    for GG in CL[0]:
        print(GG.INIT, GG.Name, GG.hp, GG.Opponents)
    for BG in CL[1]:
        print(BG.INIT, BG.Name, BG.hp, BG.Opponents)

def printHP(CL):
    print('Name      HP')
    for GG in CL[0]:
        print('{:8} {:3d}'.format(GG.Name,GG.hp))
    for BG in CL[1]:
        print('{:8} {:3d}'.format(BG.Name,BG.hp))

def ShowPairing(attacker,defender):
    print('The attacker is {:8} and the defender is {:8}'.format(attacker.Name, defender.Name))

def UpdateML_defender_hp(defender_name,damage):
    global ML

    # whether the defender is good or bad, update their hit points in the Master List.
    for x in range(0,len(ML[0])):
        if ML[0][x].Name == defender_name:
            hp_original = ML[0][x].hp
            hp_new = hp_original - damage
            ML[0][x].hp = hp_new
            print('      Good guy {:8} now has {:3d} hp'.format(ML[0][x].Name, ML[0][x].hp))
    for y in range(0,len(ML[1])):
        if ML[1][y].Name == defender_name:
            hp_original = ML[1][y].hp
            hp_new = hp_original - damage
            ML[1][y].hp = hp_new
            print('      Bad guy {:8} now has {:3d} hp'.format(ML[1][y].Name, ML[1][y].hp))

def UpdateCO_defender_hp(defender_name,damage): # update CombatOrder
    global CombatOrder

    # Search through CombatOrder for the defender name
    for dude in CombatOrder:
        if dude.Name == defender_name:
            dude.hp -= damage

def getDefHP(defender_name):
    global CombatOrder
    for dude in CombatOrder:
        if dude.Name == defender_name:
            return dude.hp

def GetCombatantAC(defender_name):
    global CombatOrder

    # Search through CO for the defender name
    for dude in CombatOrder:
        if dude.Name == defender_name:
            return dude.AC

def ShowDiceRollTotals(d4_rolls,d6_rolls,d8_rolls,d10_rolls,d12_rolls,d20_rolls):
    print('Rolled a d20 {:3} times.'.format(d20_rolls))
    print('Rolled a d12 {:3} times.'.format(d12_rolls))
    print('Rolled a d10 {:3} times.'.format(d10_rolls))
    print('Rolled a d8 {:3} times.'.format(d8_rolls))
    print('Rolled a d6 {:3} times.'.format(d6_rolls))
    print('Rolled a d4 {:3} times.'.format(d4_rolls))

def GetDefAC(defender_name):
    global ML

    for x in range(0,len(ML[0])):
        if ML[0][x].Name == defender_name:
            def_AC = ML[0][x].AC
            print('      {:8} has AC {:3d} and {:3d} hp'.format(ML[0][x].Name,def_AC, ML[0][x].hp))
    for y in range(0,len(ML[1])):
        if ML[1][y].Name == defender_name:
            def_AC = ML[1][y].AC
            print('      {:8} has AC {:3d} and {:3d} hp'.format(ML[1][y].Name,def_AC, ML[1][y].hp))
    return def_AC

def IsDead(defender_name):
    global CombatOrder

    for dude in CombatOrder:
        if dude.Name == defender_name:
            if dude.hp < 1:
                return True
            else:
                return False

def CheckDead(defender_name):
    global ML

    # whether the defender is good or bad, check to see if they're dead.
    for x in range(0,len(ML[0])):
        if ML[0][x].Name == defender_name:
            if ML[0][x].hp < 1:
                return True  # this guy is dead
            else:
                return False
    for y in range(0,len(ML[1])):
        if ML[1][y].Name == defender_name:
            if ML[1][y].hp < 1:
                return True
            else:
                return False

def RemoveOpponentFromML(attacker_name):
    global ML
    # When a combatant dies, they get removed from all Opponent lists.
    for GG in ML[0]:
        if attacker_name in GG.Opponents:
            # attacker was one of the good guys and he's in a BG.Opponent list somewhere.  Remove him from that list.
            for BG in ML[1]:
                if attacker_name in BG.Opponents:
                    BG.Opponents.remove(attacker_name)
                    print('Removed ' + attacker_name + ' from the opponent list of ' + BG.Name)
    for BG in ML[1]:
        if attacker_name in BG.Opponents:
            # attacker was one of the bad guys and he's in a GG.Opponent list somewhere.  Remove him from that list.
            for GG in ML[0]:
                if attacker_name in GG.Opponents:
                    GG.Opponents.remove(attacker_name)
                    print('Removed ' + attacker_name + ' from the opponent list of ' + GG.Name)





#_________  MAIN PROGRAM BODY _________
print()
print('####################  N E W   C O M B A T  ####################\n')
# Goals()
Tavern()

print('A fight breaks out!\n')

for squad in GG_army:
    SetUpG_List(squad) # this will create the types and add them to the overall GG list

for squad in BG_army:
    SetUpG_List(squad) # this will create the types and add them to the overall BG list

# Add any heroes?
# def __init__(self,Name,STR,DEX,CON,INT,WIS,CHR,HP,AC):
Arlo = Hero('Arlo',14,15,16,14,15,10,27,16)
Arlo.MeleeATmod = 2
Arlo.MeleeATdam_NumDice = 1
Arlo.MeleeATdam_DiceFaces = 8
Arlo.MeleeATdam_Mod = 2
Arlo.INIT = dice(1,20) + Arlo.DEXmod
GG.append(Arlo)

Miros = Hero('Miros',16,10,15,11,12,14,22,10)
Miros.MeleeATmod = 5
Miros.MeleeATdam_NumDice = 1
Miros.MeleeATdam_DiceFaces = 4
Miros.MeleeATdam_Mod = 3
Miros.INIT = dice(1,20) + Miros.DEXmod
GG.append(Miros)












# Develop Master List ML based on GG and BG lists
ML = [GG,BG]

# Set initial contents of picklist_names; we'll draw from these lists for opponent assignment.
GG_picklist_names = []
for GG in ML[0]:
    GG_picklist_names.append(GG.Name)

BG_picklist_names = []
for BG in ML[1]:
    BG_picklist_names.append(BG.Name)

# To start, randomly choose opponent pairs from the available fighter picklists.
# Update the ML record with the selected opponents.

if len(GG_picklist_names) < len(BG_picklist_names): # BG outnumber GG
    # First go through the good guys and assign a random opponent.
    for Gdude in ML[0]:
        random_BG_target_name = random.choice(BG_picklist_names)
        Gdude.Opponents.append(random_BG_target_name)
        for Bdude in ML[1]:  # now give that bad guy this good guy as an opponent
            if Bdude.Name == random_BG_target_name:
                Bdude.Opponents.append(Gdude.Name)
        BG_picklist_names.remove(random_BG_target_name)

    # Now assign an opponent to the extra bad guys
    for extra_BG in BG_picklist_names:  # Get a name from each of the extra bad guys
        for m in range(0,len(ML[1])):  # Search through the master list of bad guys.
            if ML[1][m].Name == extra_BG:  # if the name matches,
                random_GG_target = random.choice(GG_picklist_names)       # get a random good guy name
                ML[1][m].Opponents.append(random_GG_target)  # and assign it to the extra bad guy.
                # Now circle back to add these bad guy opponents to the good guys who are outnumbered.
                for n in range(0,len(ML[0])):  # Look through the master GG list
                    if ML[0][n].Name == random_GG_target:  # until you find the randomly-selected name
                        ML[0][n].Opponents.append(ML[1][m].Name) # add that BG to its opponent list.
                # GG_picklist_names.remove(random_GG_target) # so the GG has no more than 2 on 1
else:
    # This section is for when Good guys >= Bad guys.
    # First go through the bad guys and assign a random opponent.
    for Bdude in ML[1]:
        random_GG_target_name = random.choice(GG_picklist_names)
        Bdude.Opponents.append(random_GG_target_name)
        for Gdude in ML[0]:  # now give that good guy this bad guy as an opponent
            if Gdude.Name == random_GG_target_name:
                Gdude.Opponents.append(Bdude.Name)
        GG_picklist_names.remove(random_GG_target_name)

    # Now assign an opponent to the extra good guys
    for extra_GG in GG_picklist_names:  # Get a name from each of the extra good guys
        for m in range(0,len(ML[0])):  # Search through the master list of good guys.
            if ML[0][m].Name == extra_GG:  # if the name matches,
                random_BG_target = random.choice(BG_picklist_names)  # get a random bad guy name
                ML[0][m].Opponents.append(random_BG_target)  # and assign it to the extra good guy.
                # Now circle back to add these good guy opponents to the bad guys who are outnumbered.
                for n in range(0,len(ML[1])):  # Look through the master BG list
                    if ML[1][n].Name == random_BG_target:  # until you find the randomly-selected name
                        ML[1][n].Opponents.append(ML[0][m].Name) # add that GG to its opponent list.
                # BG_picklist_names.remove(random_BG_target) # so the BG has no more than 2 on 1

print()

# Now make a single list of combatants ranked from highest INIT to lowest.
CombatOrder = []
for goodguy in ML[0]:
    CombatOrder.append(goodguy)
for badguy in ML[1]:
    CombatOrder.append(badguy)

CombatOrder = sorted(CombatOrder, key=lambda hero: hero.INIT, reverse = True)

# CombatOrder contains copies of both good guy and bad guy instances from ML.  Keep the list current and ML does
# not need to be updated from here on out.

# print list of attackers and all of their potential opponents
x = 1
print('INIT  NPC Name   HP')
for attacker in CombatOrder:
    print('{:3d}   {:8}  {:3d}'.format(attacker.INIT, attacker.Name, attacker.hp))
    x += 1
print()

FightOver = False
Round = 1
AttackerCount = 1

print()

# refill picklist name lists before combat begins so they can be used for opponent reassignment when someone dies.
GG_picklist_names = []
for GG in ML[0]:
    GG_picklist_names.append(GG.Name)

BG_picklist_names = []
for BG in ML[1]:
    BG_picklist_names.append(BG.Name)

while not FightOver:
    AttackerCount = 1
    print()
    print('Round {:3d}:'.format(Round))
    for attacker in CombatOrder:
        print('   Attacker ' + str(AttackerCount))
        if attacker.hp < 1:  # attacker is dead, so go to the next attacker.
            print("      {:8} cannot attack because they're dead.".format(attacker.Name))
            AttackerDead = True
        else:  # attacker is alive and can fight!
            AttackerDead = False
            # Set the defender
            if len(attacker.Opponents) > 1: # multiple opponents available, so pick one at random
                defender_name = random.choice(attacker.Opponents)
                print('      {:8} will fight {:8}, chosen at random.'.format(attacker.Name, defender_name))
            elif len(attacker.Opponents) == 1: # only one opponent
                defender_name = attacker.Opponents[0]
                print('      {:8} will fight {:8}'.format(attacker.Name, defender_name))
            else:
                print('      {:8} has an empty opponent list, so fill it with an available opponent.'.format(attacker.Name))
                if attacker.GoodSide:
                    if len(BG_picklist_names) > 0: # there are people to choose from
                        RandomNewTarget = random.choice(BG_picklist_names)
                        defender_name = RandomNewTarget
                        print('      {:8} now has {:8} as a new BG target.'.format(attacker.Name,defender_name))
                    else:
                        FightOver = True
                        print('      There are no more potential BG targets available.  Fight is over.')
                else:
                    if len(GG_picklist_names) > 0: # there are people to choose from
                        RandomNewTarget = random.choice(GG_picklist_names)
                        defender_name = RandomNewTarget
                        print('      {:8} now has {:8} as a new GG target.'.format(attacker.Name,defender_name))
                    else:
                        FightOver = True
                        print('      There are no more potential GG targets available.  Fight is over.')
                attacker.Opponents.append(defender_name)

            # Now resolve the combat between attacker and defender.
            Base_attack_roll = dice(1,20)
            d20_rolls += 1
            AT_modifier = attacker.MeleeATmod
            Attack_roll = Base_attack_roll + AT_modifier
            if Attack_roll >= GetCombatantAC(defender_name): # It's a hit!
                damage = dice(attacker.MeleeATdam_NumDice,attacker.MeleeATdam_DiceFaces) + attacker.MeleeATdam_Mod
                
                if attacker.MeleeATdam_DiceFaces == 4:
                    for x in range(0,attacker.MeleeATdam_NumDice):
                        d4_rolls += 1
                elif attacker.MeleeATdam_DiceFaces == 6:
                    for x in range(0,attacker.MeleeATdam_NumDice):
                        d6_rolls += 1
                elif attacker.MeleeATdam_DiceFaces == 8:
                    for x in range(0,attacker.MeleeATdam_NumDice):
                        d8_rolls += 1
                elif attacker.MeleeATdam_DiceFaces == 10:
                    for x in range(0,attacker.MeleeATdam_NumDice):
                        d10_rolls += 1
                elif attacker.MeleeATdam_DiceFaces == 12:
                    for x in range(0,attacker.MeleeATdam_NumDice):
                        d12_rolls += 1

                print('      ' + attacker.Name + ' hits ' + defender_name + ' for ' + str(damage) + ' pts of damage!')
                # Get defender's HP value
                DHP = getDefHP(defender_name)
                if (DHP - damage) < 1:
                    print('      {:8} is DEAD.'.format(defender_name))
                    # Since they're dead, they aren't a viable opponent anymore, so take them out of all opponent lists
                    for zz in CombatOrder:
                        if defender_name in zz.Opponents:
                            zz.Opponents.remove(defender_name)
                            print('      {:8} has been removed from the Opponents list of {:8}.'.format(defender_name, zz.Name))

                    # Since they're dead, they should also be removed from the list of potential opponents
                    if len(GG_picklist_names) > 0:
                        for a in GG_picklist_names:
                            if a == defender_name:
                                GG_picklist_names.remove(defender_name)
                    if len(BG_picklist_names) > 0:
                        for b in BG_picklist_names:
                            if b == defender_name:
                                BG_picklist_names.remove(defender_name)

                # Finally, update the defender's hit point value as they just got tagged by the attacker!
                UpdateCO_defender_hp(defender_name,damage)
            else:
                damage = 0
                print('      {:8} misses {:8}!  No damage taken.'.format(attacker.Name,defender_name))

        AttackerCount += 1
    print()
 
    if len(GG_picklist_names) == 0:
        FightOver = True
        print('\nGame over! After {:3} rounds, all good guys have been defeated and there are {:3} bad guys remaining.'.format(Round,len(BG_picklist_names)))
        print('The survivors are: ')
        for survivor in BG_picklist_names:
            print(survivor)
        ShowDiceRollTotals(d4_rolls,d6_rolls,d8_rolls,d10_rolls,d12_rolls,d20_rolls)
    if len(BG_picklist_names) == 0:
        FightOver = True
        print('\nGame over! After {:3} rounds, all bad guys have been defeated and there are {:3} good guys remaining.'.format(Round,len(GG_picklist_names)))
        for survivor in GG_picklist_names:
            print(survivor)
        ShowDiceRollTotals(d4_rolls,d6_rolls,d8_rolls,d10_rolls,d12_rolls,d20_rolls)

    Round += 1
<<<<<<< HEAD
 
=======
>>>>>>> 4efef1eb870b4d319093858486cfdf682bd6eee9
