import random

D = {'Bulbasoar':12, 'Oddish':24, 'Belsprout':36, 'Ninetails':48, 'Pikachu':60, 'Charmainder':72}

# for x in D:
#     print(x)  # will print the keys (the Pokemon names)

# print(D['Bulbasoar'])  # will print 12

# Create a list to hold the names
tempList = []
for keyname in D:
    tempList.append(keyname)

# pick three random names from the list and show their dictionary value
for i in range(0,3):
    MyPokemon = random.choice(tempList)
    print(MyPokemon)  # Prints the name
    print(D[MyPokemon]) # Prints the value
    tempList.remove(MyPokemon)