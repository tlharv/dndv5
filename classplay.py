class character():
    def __init__(self,name):
        self.name = name
        # self.stats = [18,14,16,10,8,10] # STR, DEX, CON, INT, WIS, CHR
        self.stats = {'STR':18, 'DEX':14, 'CON':16, 'INT':10, 'WIS':8, 'CHR':10,
                        'hp':10, 'AC':10}
        self.hp = 10
        self.AC = 10
    
    


class PC(character):
    pass

class NPC(character):
    pass



Gruntor = NPC('Gruntor')
# print(Gruntor.stats[0])
print(Gruntor.stats['STR'])
print(Gruntor.stats['hp'])
Gruntor.stats['INT'] = 18
Arlo = PC('Arlo')
print(Arlo.stats['WIS'])
print(Arlo.AC)